package com.qa;

import com.aventstack.extentreports.Status;
import com.qa.reports.ExtentReport;
import com.qa.utils.TestUtils;
import io.appium.java_client.AppiumBy;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.InteractsWithApps;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.screenrecording.CanRecordScreen;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import io.appium.java_client.service.local.flags.GeneralServerFlag;
import org.apache.commons.codec.binary.Base64;
import org.apache.logging.log4j.ThreadContext;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.*;

import java.io.*;
import java.net.ServerSocket;
import java.net.URL;
import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class BaseTest {

    public BaseTest(){
        PageFactory.initElements(new AppiumFieldDecorator(getDriver()),this);
    }

    //Since driver is overridden by page classes, it's recommended to make it static
    protected static ThreadLocal<AppiumDriver> driver = new ThreadLocal<AppiumDriver>();
    protected static ThreadLocal<Properties> props = new ThreadLocal<Properties>();;
    protected static ThreadLocal<String> dateTime = new ThreadLocal<String>();
    private static ThreadLocal<String> platform = new ThreadLocal<String>();
    private static ThreadLocal<String> deviceName = new ThreadLocal<String>();

    protected static ThreadLocal <HashMap<String, String>> strings = new ThreadLocal<HashMap<String, String>>();

    private static AppiumDriverLocalService server;
    TestUtils utils = new TestUtils();

    public AppiumDriver getDriver() {
        return driver.get();
    }
    public void setDriver(AppiumDriver driver2){
        driver.set(driver2);
    }

    public Properties getProps() {
        return props.get();
    }
    public void setProps(Properties props2){
        props.set(props2);
    }

    public String getDateAndTime(){
        return dateTime.get();
    }
    public void setDateAndTime(String dateAndTime2){
        dateTime.set(dateAndTime2);
    }

    public String getPlatform() {
        return platform.get();
    }
    public void setPlatform(String platform2){
        platform.set(platform2);
    }

    public HashMap<String,String> getStrings(){ return strings.get(); }
    public void setStrings(HashMap<String,String> str2){
        strings.set(str2);
    }
    
    public String getDevicename(){return deviceName.get(); }
    public void setDeviceName(String deviceName1){
        deviceName.set(deviceName1);
    }

    @BeforeSuite
    public void beforeSuite() throws Exception {
        ThreadContext.put("ROUTINGKEY","ServerLogs");
        server = getAppiumService();
        //server = getAppiumServerDefault();
        //if (!server.isRunning()){ //It's not working as expected
        if (!checkIfAppiumServerIsRunning(4723)){
            server.start();
            server.clearOutPutStreams();    //To clear server logs to display on console
            utils.log().info("Appium server started");
        }else{
            utils.log().info("Appium server is already running");
        }
    }

    public boolean checkIfAppiumServerIsRunning(int port) throws Exception{
        boolean isAppiumServerRunning = false;
        ServerSocket socket = null;
        try{
            socket = new ServerSocket(port);
        }catch (IOException e){
            System.out.println("1");
            isAppiumServerRunning = true;
        }finally {
            if (socket != null && !socket.isClosed())
                socket.close();
        }
        return isAppiumServerRunning;
    }

    public AppiumDriverLocalService getAppiumServerDefault(){
        return AppiumDriverLocalService.buildDefaultService();
    }

    public AppiumDriverLocalService getAppiumService(){
         //HashMap<String,String> environment = new HashMap<String, String>();
         //environment.put("PATH","/Users/aisle/Library/Android/sdk/platform-tools:/Users/aisle/Library/Android/sdk/cmdlie-tools:/Library/Java/JavaVirtualMachines/jdk-15.0.2.jdk/Contents/Home/bin:/usr/local/bin:/usr/local/sbin:/usr/local/bin:/System/Cryptexes/App/usr/bin:/usr/bin:/bin:/usr/sbin:/sbin:/Library/Apple/usr/bin");

        return AppiumDriverLocalService.buildService(new AppiumServiceBuilder()
                .usingDriverExecutable(new File("/usr/local/bin/node"))
                .withAppiumJS(new File("/usr/local/lib/node_modules/appium/build/lib/main.js"))
                .usingPort(4723)
                .withArgument(GeneralServerFlag.SESSION_OVERRIDE)
                .withArgument(GeneralServerFlag.LOCAL_TIMEZONE)
                .withLogFile(new File("ServerLogs/server.log")));
    }

    @AfterSuite
    public void afterSuite(){
        server.stop();
        utils.log().info("Appium server stopped");
    }

    @BeforeMethod
    public void beforeMethod(){
        ((CanRecordScreen)getDriver()).startRecordingScreen();
    }

    @AfterMethod
    public synchronized void afterMethod(ITestResult result){
        String media = ((CanRecordScreen)getDriver()).stopRecordingScreen();

        /*----  To check the status of the method wheather it's Fail or Pass    -----
        * int CREATED = -1;     int SUCCESS = 1;        int FAILURE = 2;        int SKIP = 3;
        * int SUCCESS_PERCENTAGE_FAILURE = 4;     int STARTED = 16;
        */
        if (result.getStatus()==2){
            Map<String, String> patrams = result.getTestContext().getCurrentXmlTest().getAllParameters();

            String dir = "ScreenRecording"+ File.separator+ patrams.get("platformName")+"_"+patrams.get("platformVersion")+
                    "_"+patrams.get("deviceName")+File.separator+getDateAndTime()+File.separator+
                    result.getTestClass().getRealClass().getSimpleName();
            File videoDir = new File(dir);

            synchronized (videoDir){
                if (!videoDir.exists()){
                    videoDir.mkdirs();  /*---- To create multiple files, if it exists it'll over write it  ----*/
                }
            }
            try {
                FileOutputStream stream = new FileOutputStream(videoDir+File.separator+result.getName()+".mp4");
                stream.write(Base64.decodeBase64(media));
            } catch (FileNotFoundException e) {
                throw new RuntimeException(e);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    @Parameters({"emulator","platformName","platformVersion","deviceName","udid","systemPort","chromeDriverPort",
                    "wdaLocalPort","webkitDebugPort"})
    @BeforeTest
    public void beforeTest(@Optional("androidOnly") String emulator, String platformName,String platformVersion,String deviceName,String udid,
                           @Optional("androidOnly") String systemPort,
                           @Optional("androidOnly") String chromeDriverPort,
                           @Optional("iOSOnly") String webkitDebugPort,
                           @Optional("iOSOnly") String wdaLocalPort) throws Exception {

        URL url;
        setDateAndTime(utils.dateTime());
        setPlatform(platformName);
        setDeviceName(deviceName);
            InputStream stringsis = null;
            InputStream inputStream = null;
            Properties props = new Properties();
            AppiumDriver driver;

            //Directory for logging file
        String strFile = "logs"+File.separator+platformName+"_"+getDevicename();
        File logFile = new File(strFile);
        if (!logFile.exists())
            logFile.mkdir();
        ThreadContext.put("ROUTINGKEY",strFile);

        try{
            props = new Properties();
            String propFileName = "config.properties";
            String xmlFileName = "strings/strings.xml";

            inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
            props.load(inputStream);
            setProps(props); /* Assigning to the global varialble after loading*/

            stringsis = getClass().getClassLoader().getResourceAsStream(xmlFileName);
            setStrings(utils.parseStringXML(stringsis));

//            stringsis = getClass().getClassLoader().getResourceAsStream(xmlFileName);
//            setStrings(utils.parseStringXML(stringsis));

            /*
                If the file is not in Class path, then you might use FileInputStream instead of InputStream.
                It's a good practice to keep all resource or src/test/resources folder
            */
            DesiredCapabilities desiredCaps = new DesiredCapabilities();
            //Device specific parameters are passed from xml
            desiredCaps.setCapability("platformName" ,platformName);
            desiredCaps.setCapability("deviceName",deviceName);
            desiredCaps.setCapability("udid", udid);
            url = new URL(props.getProperty("appiumURL")+"4723"); //When we doing parallel exe in single server

            switch (platformName){
                case "Android":
                    //Automation parameters are passed from config.properties file
                    desiredCaps.setCapability("automationName",props.getProperty("androidAutomationName"));
                    desiredCaps.setCapability("appPackage",props.getProperty("androidAppPackage"));
                    desiredCaps.setCapability("appActivity",props.getProperty("androidAppActivity"));
                    desiredCaps.setCapability("systemPort",systemPort);
                    desiredCaps.setCapability("chromeDriverPort",chromeDriverPort);
                    if (emulator.equalsIgnoreCase("true")) {
                        desiredCaps.setCapability("platformVersion", platformVersion);
                        desiredCaps.setCapability("avd", deviceName);
                    }
                    //CASE 1:
                    //***** In Below case appUrl is null because of getClassLoader()
                    //URL appUrl = getClass().getClassLoader().getResource(props.getProperty("androidAppLocation"));
                    String androidAppUrl = getClass().getResource(props.getProperty("androidAppLocation")).getFile();
/*
            //CASE 2:
            String appUrl = System.getProperty("user.dir")+File.separator+"src"+File.separator+"resources"+File.separator+
                            "app"+File.separator+"Android.SauceLabs.Mobile.Sample.app.2.7.1.apk";

 */

                    desiredCaps.setCapability("app",androidAppUrl);

                    /* When we're using 2 appium server */
                    //url = new URL(props.getProperty("appiumURL")+"4723");

                    driver = new AndroidDriver(url, desiredCaps);
                    break;

                case "iOS":
                    desiredCaps.setCapability("automationName",props.getProperty("iOSAutomationName"));
                    //String iOSAppUrl = getClass().getResource(props.getProperty("iOSAppLocation")).getFile();
                    //desiredCaps.setCapability("app",iOSAppUrl);
                    desiredCaps.setCapability("webkitDebugPort",webkitDebugPort);
                    desiredCaps.setCapability("wdaLocalPort",wdaLocalPort);
                    desiredCaps.setCapability("bundleId",props.getProperty("iOSBundleId"));
                    /* When we're using 2 appium server */
                    //url = new URL(props.getProperty("appiumURL")+"4724");
                    driver = new IOSDriver(url,desiredCaps);
                    break;

                default:
                    throw new Exception("Invalid platform: "+platformName);
            }
            setDriver(driver); //assigning it to global
            String sessionId = getDriver().getSessionId().toString();
        }catch (Exception e){
            e.printStackTrace();
            /*  If we don't throw the exception the program will not exit if there is
                some issue while creating the driver session                         */
            throw e;
        }finally {
            if (inputStream!=null){
                inputStream.close();
            }
            if (stringsis!=null){
                stringsis.close();
            }
        }
    }

    @AfterTest
    public void afterTest(){
        getDriver().quit();
        utils.log().info("Terminated the driver");
    }

    //Usage of Explicit Wait
    public void waitForVisibility(WebElement e){
        WebDriverWait wait = new WebDriverWait(getDriver(),Duration.ofSeconds(TestUtils.WAIT));
        wait.until(ExpectedConditions.visibilityOf(e));
    }

    public void click(WebElement e){
        waitForVisibility(e);
        e.click();
    }

    public void click(WebElement e, String msg){
        waitForVisibility(e);
        utils.log().info(msg);
        ExtentReport.getTest().log(Status.INFO,msg);
        e.click();
    }
    public void sendKeys(WebElement ele, String text){
        waitForVisibility(ele);
        ele.sendKeys(text);
    }

    public void sendKeys(WebElement ele, String text, String msg){
        waitForVisibility(ele);
        ExtentReport.getTest().log(Status.INFO,msg);
        utils.log().info(msg);
        ele.sendKeys(text);

    }

    public void clear(WebElement e){
        waitForVisibility(e);
        e.clear();
        utils.log().info("Cleared the text field: "+getText(e));
    }

    public String getAttribute(WebElement e,String attribute){
        waitForVisibility(e);
        return e.getAttribute(attribute);
    }

    public String getText(WebElement e){
        switch (getPlatform()) {
            case "Android":
                return getAttribute(e, "text");
            case "iOS":
                return getAttribute(e,"label");
        }
        return null;
    }

    public String getText(WebElement e, String msg){
        String txt = null;
        switch (getPlatform()) {
            case "Android":
                txt = getAttribute(e,"text");
                break;
            case "iOS":
                txt = getAttribute(e,"label");
                break;
        }
        ExtentReport.getTest().log(Status.INFO,msg);
        utils.log().info(msg+txt);
        return txt;
    }

    public void closeApp(){
        switch (getPlatform()){
            case "Android":
                ((InteractsWithApps)getDriver()).terminateApp(getProps().getProperty("androidAppPackage"));
                utils.log().info("Terminated the app");
                break;
            case "iOS":
                ((InteractsWithApps)getDriver()).terminateApp(getProps().getProperty("iOSBundleId"));
                utils.log().info("Terminated the app");
        }
    }

    public void launchApp(){
        switch (getPlatform()){
            case "Android":
                ((InteractsWithApps)getDriver()).activateApp(getProps().getProperty("androidAppPackage"));
                utils.log().info("Launched the app");
                break;
            case "iOS":
                ((InteractsWithApps)getDriver()).activateApp(getProps().getProperty("iOSBundleId"));
                utils.log().info("Launched the app");
        }
    }

    public WebElement scrollToElement(){
        // Scroll to the element
        /*
        getDriver().findElement(AppiumBy.androidUIAutomator(
                "new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(new UiSelector().text(\"test-Price\"))"
        ));



        // Wait for the element to be visible after scrolling
        return wait.until(ExpectedConditions.visibilityOfElementLocated(AppiumBy.accessibilityId("test-Price")));

         */
        // Example: Scroll forward
       return getDriver().findElement(AppiumBy.androidUIAutomator("new UiScrollable(new UiSelector().scrollable(true)).scrollForward();"));

    }

    public void iOSScrollDownToElement(){
    RemoteWebElement scrollEle = (RemoteWebElement)getDriver().findElement(AppiumBy.className("XCUIElementTypeScrollView"));
        String elementID = scrollEle.getId();
        HashMap<String,String> scrollObject = new HashMap<>();
        scrollObject.put("element",elementID);
        scrollObject.put("direction","down");
        getDriver().executeScript("mobile:scroll",scrollObject);
    }

    public void iOSScrollUpToElement(){
        RemoteWebElement parentEle = (RemoteWebElement)getDriver().findElement(AppiumBy.className("XCUIElementTypeScrollView"));
        String parentEleId = parentEle.getId();
        HashMap<String,String> scrollObject = new HashMap<>();
        scrollObject.put("element",parentEleId);
        scrollObject.put("direction","up");

        //****** Scroll to an Element *****//
        //scrollObject.put("predicateString", "label == 'ADD TO CART'");
        //scrollObject.put("name","test-ADD TO CART");

        //If we know the id of the element, parent element is not needed toscroll
        getDriver().executeScript("mobile:scroll",scrollObject);
    }

}
