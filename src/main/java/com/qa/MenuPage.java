package com.qa;

import com.qa.pages.SettingsPage;
import com.qa.utils.TestUtils;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import org.openqa.selenium.WebElement;

public class MenuPage extends BaseTest{
    TestUtils utils = new TestUtils();
    @AndroidFindBy(xpath = "//android.view.ViewGroup[@content-desc=\"test-Menu\"]/android.view.ViewGroup/android.widget.ImageView")
    @iOSXCUITFindBy(id = "test-Menu")
        private WebElement settingsButton;
    public SettingsPage pressSettingsButton(){
        utils.log().info("Press Settings Button");
        click(settingsButton);
        return new SettingsPage();
    }
}
