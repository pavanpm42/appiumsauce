package com.qa.pages;

import com.qa.MenuPage;
import com.qa.utils.TestUtils;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import org.openqa.selenium.WebElement;

public class ProductsPage extends MenuPage {
    TestUtils utils = new TestUtils();
    @AndroidFindBy(xpath = "//android.widget.ScrollView[@content-desc=\"test-PRODUCTS\"]/preceding-sibling::android.view.ViewGroup//android.widget.TextView")
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name=\"test-Toggle\"]/parent::*[1]/preceding-sibling::XCUIElementTypeStaticText")
        private WebElement productTitleTxt;

    @AndroidFindBy(xpath = "(//android.widget.TextView[@content-desc=\"test-Item title\"])[1]")
    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeStaticText[@name=\"test-Item title\"])[1]")
        private WebElement slbTitle;

    @AndroidFindBy(xpath = "(//android.widget.TextView[@content-desc=\"test-Price\"])[1]")
    @iOSXCUITFindBy(xpath = "(//XCUIElementTypeStaticText[@name=\"test-Price\"])[1]")
        private WebElement slbPrice;



    public String getTitle(){
        return getText(productTitleTxt);
    }

    public String getSlbTitle(){
        String title = getText(slbTitle,"title is - ");
        return title;
    }

    public String getSlbPrice(){
        String price = getText(slbPrice,"Price is - ");
        return price;
    }

    public ProductDetailsPage pressSlbTitle(){
        click(slbTitle,"Pressed on SLB Title URL");
        return new ProductDetailsPage();
    }

}
