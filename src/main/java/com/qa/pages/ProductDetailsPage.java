package com.qa.pages;

import com.qa.MenuPage;
import com.qa.utils.TestUtils;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import org.openqa.selenium.WebElement;

public class ProductDetailsPage extends MenuPage {
    TestUtils utils = new TestUtils();
    @AndroidFindBy(xpath = "//android.view.ViewGroup[@content-desc=\"test-Description\"]/android.widget.TextView[1]")
    @iOSXCUITFindBy(id = "Sauce Labs Backpack")
        private WebElement slbTitle;
    @AndroidFindBy(xpath = "//android.view.ViewGroup[@content-desc=\"test-Description\"]/android.widget.TextView[2]")
    @iOSXCUITFindBy(id = "carry.allTheThings() with the sleek, streamlined Sly Pack that melds uncompromising style with unequaled laptop and tablet protection.")
        private WebElement slbTxt;

    @AndroidFindBy(accessibility = "test-BACK TO PRODUCTS")
    @iOSXCUITFindBy(accessibility = "test-BACK TO PRODUCTS")
        private WebElement backToProdpageBtn;

    @AndroidFindBy(accessibility = "test-Price")
        private WebElement slbPrice;

    @AndroidFindBy(xpath = "//android.view.ViewGroup[@content-desc=\"test-ADD TO CART\"]/android.widget.TextView")
    @iOSXCUITFindBy(iOSNsPredicate = "label == \"BACK TO PRODUCTS\" AND name == \"test-BACK TO PRODUCTS\"")
        private WebElement addToCart;
    public String getSlbTitle(){
        String title =  getText(slbTitle);
        return title;
    }

    public String getSlbTxt(){
        return getText(slbTxt);
    }

    public ProductsPage pressOnBackToProdPageBtn() {
        click(backToProdpageBtn,"Navigate Back To Product Page Button");
        return new ProductsPage();
    }
    public String getSlbPrice(){
        String price = getText(slbPrice,"Price is : ");
        return price;
    }

    public String getAddToCart(){
        return getText(addToCart);
    }
    public void scrollToAddCartButton(){
        utils.log().info("Before scrolling to Element");
        scrollToElement();
        utils.log().info("After scrolling to Element");
    }

    public void clickonAddToCart(){
        click(addToCart,"Clicked on :");
    }

    public void scrollPageDown(){
        iOSScrollDownToElement();
    }

    public void scrollPageUp(){
        iOSScrollUpToElement();
    }

    public Boolean isAddToCartButtonDisplayed(){
        return addToCart.isDisplayed();
    }
}
