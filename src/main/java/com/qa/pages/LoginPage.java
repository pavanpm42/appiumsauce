package com.qa.pages;

import com.qa.BaseTest;
import com.qa.utils.TestUtils;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class LoginPage extends BaseTest {
    TestUtils utils = new TestUtils();
    @AndroidFindBy(accessibility = "test-Username")
    @iOSXCUITFindBy(id = "test-Username")
        private WebElement userNameTxtFld;
    @AndroidFindBy(accessibility = "test-Password")
    @iOSXCUITFindBy(id = "test-Password")
        private WebElement passwordTxtFld;
    @AndroidFindBy(accessibility = "test-LOGIN")
    @iOSXCUITFindBy(id = "test-LOGIN")
        private WebElement loginBtn;
    @AndroidFindBy(xpath = "//android.view.ViewGroup[@content-desc=\"test-Error message\"]/android.widget.TextView")
    @iOSXCUITFindBy(xpath = "//XCUIElementTypeOther[@name=\"test-Error message\"]/child::XCUIElementTypeStaticText")
        private WebElement errTxt;

    public LoginPage enterUserName(String username){
        clear(userNameTxtFld);
        sendKeys(userNameTxtFld, username, "login with username is ");
        return this;
    }

    public LoginPage enterPassword(String pwd){
        clear(passwordTxtFld);
        //utils.log().info("Password is "+pwd);
        sendKeys(passwordTxtFld,pwd,"Password is ");
        return this;
    }

    public String getErrTxt(){
        return getText(errTxt,"Error text is = ");

    }

    public ProductsPage tapOnLoginButton(){
        click(loginBtn,"Press Login Button");
        return new ProductsPage();
    }

    public ProductsPage login(String username,String password){
        enterUserName(username);
        enterPassword(password);
        return tapOnLoginButton();
    }

}