package com.qa.tests;

import com.qa.BaseTest;
import com.qa.pages.LoginPage;
import com.qa.pages.ProductDetailsPage;
import com.qa.pages.ProductsPage;
import com.qa.pages.SettingsPage;
import com.qa.utils.TestUtils;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;

public class ProductTests extends BaseTest {

    LoginPage loginPage;
    ProductsPage productsPage;
    SettingsPage settingsPage;
    ProductDetailsPage productDetailsPage;
    String dataFilename = "data/loginUsers.json";
    JSONObject loginUsers;
    TestUtils utils = new TestUtils();
    @BeforeClass
    public void beforeClass() throws IOException {
        InputStream datais = null;
        //To read JSON Objects
        try{
            datais = getClass().getClassLoader().getResourceAsStream(dataFilename);
            JSONTokener tokener = new JSONTokener(datais);
            loginUsers = new JSONObject(tokener);
        }catch (Exception e){
            e.printStackTrace();
            throw e;

        }finally {
            //if (datais!=null)
                //datais.close();
        }
        closeApp();
        launchApp();
    }

    @BeforeMethod
    public void beforeMethod(Method method){
        utils.log().info("\n"+"***** Starting test: "+method.getName()+" *****"+"\n");
        loginPage = new LoginPage();
        productsPage = loginPage.login(loginUsers.getJSONObject("validUser").getString("username"),
                loginUsers.getJSONObject("validUser").getString("password"));
    }

    @AfterMethod
    public void afterMethod(){
        productDetailsPage.pressOnBackToProdPageBtn();
        loginPage = productsPage.pressSettingsButton().pressLogoutButton(); //logout from product page
    }

    @Test
    public void validateProductonProductsPage(){
        SoftAssert sa = new SoftAssert();

        String slbTitle = productsPage.getSlbTitle();
        sa.assertEquals(slbTitle,getStrings().get("products_page_slb_title"));

        String slbPrice = productsPage.getSlbPrice();
        sa.assertEquals(slbPrice,getStrings().get("products_price"));

        settingsPage = productsPage.pressSettingsButton();
        loginPage = settingsPage.pressLogoutButton();
        sa.assertAll();
    }

    @Test
    public void validateProductOnProductsDetailsPage(){
        //String parentWindow = driver.getWindowHandle();
        SoftAssert sa = new SoftAssert();

        productDetailsPage=productsPage.pressSlbTitle();

        String slbTitle= productDetailsPage.getSlbTitle();
        sa.assertEquals(slbTitle,getStrings().get("products_page_slb_title"));

        String slbDescText = productDetailsPage.getSlbTxt();
        sa.assertEquals(slbDescText,getStrings().get("products_page_desc_text"));

        // *************** Android  ******************//
        /*
        productDetailsPage.scrollToAddCartButton();
        productDetailsPage.clickonAddToCart();
        String slbPrice = productsPage.getSlbPrice();
        sa.assertEquals(slbPrice,strings.get("products_price"));
         */

        // *************** iOS  ******************//
        productDetailsPage.scrollPageDown();
        sa.assertTrue(productDetailsPage.isAddToCartButtonDisplayed());
        productDetailsPage.scrollPageUp();
        sa.assertAll();
    }
}
