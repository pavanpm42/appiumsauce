package com.qa.tests;

import com.qa.BaseTest;
import com.qa.pages.LoginPage;
import com.qa.pages.ProductsPage;
import com.qa.utils.TestUtils;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;

public class LoginTests extends BaseTest {
    LoginPage loginPage;
    ProductsPage productsPage;
    String dataFilename = "data/loginUsers.json";
    JSONObject loginUsers;
    TestUtils utils = new TestUtils();
    @BeforeClass
    public void beforeClass() throws IOException {
        InputStream datais = null;
        //To read JSON Objects
        try{
            datais = getClass().getClassLoader().getResourceAsStream(dataFilename);
            JSONTokener tokener = new JSONTokener(datais);
            loginUsers = new JSONObject(tokener);
        }catch (Exception e){
            e.printStackTrace();
            throw e;

        }finally {
            if (datais!=null)
                datais.close();
        }
        closeApp();
        launchApp();
    }

    @BeforeMethod
    public void beforeMethod(Method method){
        utils.log().info("LoginTest before method");
        utils.log().info("\n"+"***** Starting test: "+method.getName()+" *****"+"\n");
        loginPage = new LoginPage();
    }

    @AfterMethod
    public void afterMethod(){
        utils.log("LoginTest after method");
    }

    @Test
    public void invalidUsername(){
        loginPage.enterUserName(loginUsers.getJSONObject("invalidUser").getString("username"));
        loginPage.enterPassword(loginUsers.getJSONObject("invalidUser").getString("password"));
        loginPage.tapOnLoginButton();

        String actualErrText = loginPage.getErrTxt();
        String expectedErrText = getStrings().get("err_invalid_username_or_password");
        utils.log().info("Actual error text - "+actualErrText+"\n"+"Expected error text - "+expectedErrText);
        Assert.assertEquals(actualErrText,expectedErrText);
    }

    @Test
    public void invalidPassword(){
        loginPage.enterUserName(loginUsers.getJSONObject("invalidPassword").getString("username"));
        loginPage.enterPassword(loginUsers.getJSONObject("invalidPassword").getString("password"));
        loginPage.tapOnLoginButton();

        String actualErrText = loginPage.getErrTxt();
        String expectedErrText = getStrings().get("err_invalid_username_or_password");
        utils.log().info("Actual error text - "+actualErrText+"\n"+"Expected error text - "+expectedErrText);
        Assert.assertEquals(actualErrText,expectedErrText);
    }

    @Test
    public void successfulLogin(){
        loginPage.enterUserName(loginUsers.getJSONObject("validUser").getString("username"));
        loginPage.enterPassword(loginUsers.getJSONObject("validUser").getString("password"));
        productsPage = loginPage.tapOnLoginButton();

        String actualProdTxt = productsPage.getTitle();
        String expectedProdTxt = getStrings().get("product_title");
        utils.log().info("Actual product title - "+actualProdTxt+"\n"+"Expected product title - "+expectedProdTxt);
        Assert.assertEquals(actualProdTxt,expectedProdTxt);
    }
}
